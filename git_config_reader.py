import os
import configparser
import logging
import re

# Path to the git configuration file
GIT_CONFIG_PATH = os.path.expanduser("~/.config/git/config")

# Setup logging
logger = logging.getLogger(__name__)

def read_git_config(debug=False):
    if os.path.exists(GIT_CONFIG_PATH):
        config = configparser.ConfigParser()
        config.read(GIT_CONFIG_PATH)
        if debug:
            logging.basicConfig(level=logging.INFO)
        return config
    else:
        logger.error("Git config file not found")
        return None

def get_user_info(path):
    try:
        # Remove surrounding quotes from path if present
        path = path.strip('"')
        
        resolved_path = os.path.realpath(path)
        if os.path.exists(resolved_path):
            included_config = configparser.ConfigParser()
            included_config.read(resolved_path)
            user_email = included_config.get('user', 'email', fallback=None)
            user_name = included_config.get('user', 'name', fallback=None)
            return user_email, user_name
        else:
            logger.error("Included config file does not exist at resolved path: %s", resolved_path)
            return None, None
    except Exception as e:
        logger.error("Error accessing included config file: %s", str(e))
        return None, None

def main(debug=False):
    git_config = read_git_config(debug)
    if git_config:
        # Get all the section names
        sections = git_config.sections()
        
        # Iterate through each section to find matching includeIf condition
        for section in sections:
            match = re.match(r'includeIf "gitdir:~/(.*?)/\*\*', section)
            if match:
                folder_path = match.group(1)
                current_folder_path = os.getcwd()
                include_if_path = git_config.get(section, 'path', fallback=None)
                if current_folder_path.startswith(os.path.expanduser(f"~/{folder_path}/")):
                    user_email, user_name = get_user_info(include_if_path)
                    if user_email and user_name:
                        logger.info(f"User Email: {user_email}")
                        logger.info(f"User Name: {user_name}")
                        return user_email, user_name
                    else:
                        logger.error("Failed to read user email and name from included config")
                        return None, None
        else:
            logger.info("No matching includeIf condition found for the current folder")
            return None, None

if __name__ == "__main__":
    main(debug=True)  # Enable debug logging

