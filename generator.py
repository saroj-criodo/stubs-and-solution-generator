#!/usr/bin/env python

import os
import yaml
import re
import logging
import shutil
import argparse

# Define color codes for logging
RESET = "\033[0m"
BOLD = "\033[1m"
YELLOW = "\033[33m"
BLUE = "\033[34m"
WHITE = "\033[37m"
BAR_LIMIT = 100


# Define custom log functions
def log_code_block(section, operation, file, *blocks):
    logger = logging.getLogger('code_operations')  # Specific logger for code operations
    logger.info(f"{section}: {operation} -> {file}:")
    for block in blocks:
        logger.info(f"{BOLD}{BLUE}--- Code Block Start ---{RESET}")
        for line in block.split("\n"):
            logger.info(YELLOW + line + RESET)
        logger.info(f"{BOLD}{BLUE}--- Code Block End ---{RESET}")


def log_file_not_found(section, operation, file):
    logging.warning(f"{section}: {operation} -> {file}: File not found")


def log_warning(message):
    logging.warning(message)


def log_debug_info(message):
    logging.debug(message)


def load_config(config_path):
    """
    Load YAML configuration from a file.

    Args:
        config_path (str): Path to the configuration file.

    Returns:
        dict: Loaded YAML configuration.
    """
    with open(config_path, 'r') as file:
        return yaml.safe_load(file)


def apply_global_config(config, section):
    """
    Update section configuration with global settings. Handles missing keys or sections.

    Args:
        config (dict): Configuration dictionary.
        section (str): Section name.

    Returns:
        tuple: Tuple containing include patterns, ignore patterns, removal lines, and remove code blocks.
    """
    global_config = config.get('GLOBAL', {})
    section_config = config.get(section, {})

    # Ensure default values for missing keys or None values
    include_patterns = global_config.get('INCLUDE', []) or []
    ignore_patterns = global_config.get('IGNORE', []) or []

    # Get REMOVE-LINES from global and section config
    global_remove_lines = global_config.get('REMOVE-LINES', {}) or {}
    section_remove_lines = section_config.get('REMOVE-LINES', {}) or {}

    # Get REMOVE-CODE-BLOCK from global and section config
    global_remove_code_blocks = global_config.get('REMOVE-CODE-BLOCK', {}) or {}
    section_remove_code_blocks = section_config.get('REMOVE-CODE-BLOCK', {}) or {}

    # Get REPLACE-CODE-BLOCK from global and section config
    global_replace_code_blocks = global_config.get('REPLACE-CODE-BLOCK', {}) or {}
    section_replace_code_blocks = section_config.get('REPLACE-CODE-BLOCK', {}) or {}

    # Merge REMOVE-LINES dictionaries
    remove_lines = {**global_remove_lines, **section_remove_lines}

    # Merge REMOVE-CODE-BLOCK dictionaries
    remove_code_blocks = {**global_remove_code_blocks, **section_remove_code_blocks}

    # Merge REPLACE-CODE-BLOCK dictionaries
    replace_code_blocks = {**global_replace_code_blocks, **section_replace_code_blocks}

    return include_patterns, ignore_patterns, remove_lines, remove_code_blocks, replace_code_blocks


def file_matches(file, patterns, exclude_patterns=[]):
    """
    Check if file matches any of the include patterns and none of the exclude patterns.

    Args:
        file (str): File path.
        patterns (list): List of include patterns.
        exclude_patterns (list): List of exclude patterns.

    Returns:
        bool: True if the file matches any include pattern and none of the exclude patterns, False otherwise.
    """
    return any(re.match(pattern, file) for pattern in patterns) and not any(
        re.match(pattern, file) for pattern in exclude_patterns)


def remove_lines_from_file(file_path, remove_lines):
    """
    Remove specified lines from a file.

    Args:
        file_path (str): Path to the file.
        remove_lines (list): List of lines to be removed.
    """
    if not os.path.exists(file_path):
        logging.warning(f"File not found for removal rules: {file_path}")
        return

    with open(file_path, 'r', encoding='utf-8', errors="ignore") as file:
        lines = file.readlines()

    with open(file_path, 'w', encoding='utf-8', errors="ignore") as file:
        for line in lines:
            if line.strip() not in remove_lines:
                file.write(line)


def remove_code_block_from_file(file_path, remove_code_blocks):
    """
    Remove specified code blocks from a file.

    Args:
        file_path (str): Path to the file.
        remove_code_blocks (str): Strings of code blocks to be removed.
    """

    # Remove whitespaces from each line of the code block string and convert it to a list of lines
    remove_code_block_lines = list(map(lambda line: "".join(line.split()), remove_code_blocks.strip().split('\n')))

    if not os.path.exists(file_path):
        logging.warning(f"File not found for code block removal: {file_path}")
        return

    with open(file_path, 'r', encoding='utf-8', errors="ignore") as file:
        file_lines = file.readlines()

    # Function to remove whitespaces from each line
    file_lines_without_whitespace = ["".join(line.split()) for line in file_lines]

    # Find the range where remove_code_block_lines exactly starts and ends
    for i in range(len(file_lines_without_whitespace)):
        if all(remove_code_block_lines[j] == file_lines_without_whitespace[i + j] for j in
               range(len(remove_code_block_lines))):
            start_index = i
            end_index = i + len(remove_code_block_lines) - 1
            break
    else:
        # If the code block is not found, return without making changes
        logging.warning("Code block not found in the file.")
        return

    # Remove the code block from file_lines
    del file_lines[start_index:end_index + 1]

    # Write the modified file_lines back to the file
    with open(file_path, 'w', encoding='utf-8', errors="ignore") as file:
        file.writelines(file_lines)


def replace_code_block_in_file(file_path, new_code_block=None, old_code_block=None):
    """
    Replace specified code blocks in a file or replace the entire file content with the new code block.

    Args:
        file_path (str): Path to the file.
        old_code_block (str, optional): String representing the code block to be replaced.
        new_code_block (str): String representing the replacement code block.
    """
    if not os.path.exists(file_path):
        logging.warning(f"File not found for code block replacement: {file_path}")
        return

    if old_code_block is None and new_code_block is not None:
        # Replace the entire file content with the new_code_block
        with open(file_path, 'w', encoding='utf-8', errors="ignore") as file:
            file.write(new_code_block)
        return
    if old_code_block is None and new_code_block is None:
        with open(file_path, 'w', ) as file:
            pass
        return

    # Remove whitespaces from each line of the code block strings and convert them to a list of lines
    old_code_block_lines = list(map(lambda line: "".join(line.split()), old_code_block.strip().split('\n')))

    with open(file_path, 'r', encoding='utf-8', errors="ignore") as file:
        file_lines = file.readlines()

    # Function to remove whitespaces from each line
    file_lines_without_whitespace = ["".join(line.split()) for line in file_lines]

    # Find the range where old_code_block_lines exactly match in file_lines_without_whitespace
    for i in range(len(file_lines_without_whitespace)):
        if all(old_code_block_lines[j] == file_lines_without_whitespace[i + j] for j in
               range(len(old_code_block_lines))):
            start_index = i
            end_index = i + len(old_code_block_lines) - 1
            break
    else:
        # If the code block is not found, log a warning and return without making changes
        logging.warning("Old code block not found in the file.")
        return

    # Replace the old code block with the new one in file_lines
    file_lines[start_index:end_index + 1] = new_code_block.split('\n')

    # Write the modified file_lines back to the file
    with open(file_path, 'w', encoding='utf-8', errors="ignore") as file:
        file.writelines(file_lines)


def scan_and_process_directory(config, section, base_path, output_base, debug_mode):
    """
    Scan directory for files matching the combined global and section-specific include/exclude patterns and
    process them.

    Args:
        config (dict): Configuration dictionary.
        section (str): Section name.
        base_path (str): Base directory path to scan.
        output_base (str): Output directory path.
        debug_mode (bool): Debug mode flag.
    """
    # Set up output directory
    me_name = config.get("ME", "ME")
    module_name = config.get("MODULE", "MODULE")
    output_dir = os.path.join(output_base, f"{me_name}/{module_name}_{section.upper()}/")

    include_patterns, ignore_patterns, remove_lines, remove_code_blocks, replace_code_blocks = apply_global_config(
        config, section)

    # Create output directory if not in debug mode
    if not debug_mode and not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # Process files
    for root, dirs, files in os.walk(base_path):
        for file in files:
            file_path = os.path.join(root, file)
            relative_path = os.path.relpath(file_path, start=base_path)
            if file_matches(relative_path, include_patterns, ignore_patterns):
                if debug_mode:
                    logging.info(f"{section}: INCLUDE -> {relative_path}")
                else:
                    dest_path = os.path.join(output_dir, relative_path)
                    dest_dir = os.path.dirname(dest_path)
                    if not os.path.exists(dest_dir):
                        os.makedirs(dest_dir)
                    shutil.copy(file_path, dest_path)

    # Perform REMOVE-LINES operation after copying files and if not in debug mode
    if not debug_mode:
        for key, value in remove_lines.items():
            file = output_dir + key
            if os.path.exists(file):
                remove_lines_from_file(file, value)
                log_code_block(section, "REMOVE-LINES", file, *value)
            else:
                log_file_not_found(section, "REMOVE-LINES", file)

        # Perform REMOVE-CODE-BLOCK operation after copying files and if not in debug mode
        for key, value in remove_code_blocks.items():
            file = output_dir + key
            if os.path.exists(file):
                remove_code_block_from_file(file, value)
                log_code_block(section, "REMOVE-CODE-BLOCK", file, value)
            else:
                log_file_not_found(section, "REMOVE-CODE-BLOCK", file)

    # Replace code blocks based on the configuration if not in debug mode
    if not debug_mode:
        for key, value in replace_code_blocks.items():
            file = output_dir + key
            if os.path.exists(file):
                if isinstance(value, list) and len(value) >= 1:
                    for replace_block in value:
                        if isinstance(replace_block, dict):
                            old_code_block = replace_block.get('search')
                            new_code_block = replace_block.get('replace')
                            if old_code_block and new_code_block:
                                log_code_block(section, "REPLACE-CODE-BLOCK", file, old_code_block, new_code_block)
                                # position of params matters
                                replace_code_block_in_file(file, new_code_block, old_code_block)
                            elif new_code_block:
                                log_code_block(section, "REPLACE-FILE_WITH-CODE-BLOCK", file, new_code_block)
                                replace_code_block_in_file(file, new_code_block)
                            elif not new_code_block:
                                logging.info(f"{section}: REPLACE-WITH-EMPTY-FILE -> {file}:")
                                replace_code_block_in_file(file, None)
                            else:
                                log_warning(
                                    f"{section}: REPLACE-CODE-BLOCK -> {key}: Insufficient data in value dictionary")
                        else:
                            log_warning(f"{section}: REPLACE-CODE-BLOCK -> {key}: Expected dictionary in value list")
                else:
                    log_warning(f"{section}: REPLACE-CODE-BLOCK -> {key}: Expected list in value")
            else:
                log_file_not_found(section, "REPLACE-CODE-BLOCK", file)

    # Display REMOVE-LINES, REMOVE-CODE-BLOCK, and REPLACE-CODE-BLOCK data in debug mode
    if debug_mode:
        if remove_lines:
            for key, value in remove_lines.items():
                file = base_path + key
                if os.path.exists(file):
                    for x in value:
                        log_code_block(section, "REMOVE-LINES", file, x)
                else:
                    log_file_not_found(section, "REMOVE-LINES", file)

        if remove_code_blocks:
            for key, value in remove_code_blocks.items():
                file = base_path + key
                if os.path.exists(file):
                    log_code_block(section, "REMOVE-CODE-BLOCK", file, value)
                else:
                    log_file_not_found(section, "REMOVE-CODE-BLOCK", file)

        if replace_code_blocks:
            for key, value in replace_code_blocks.items():
                file = base_path + key
                if os.path.exists(file):
                    for x in value:
                        if isinstance(x, dict) and 'search' in x and 'replace' in x:
                            log_code_block(section, "REPLACE-CODE-BLOCK: OLD-CODE", file, x['search'])
                            log_code_block(section, "REPLACE-CODE-BLOCK: NEW-CODE", file, x['replace'])
                        elif not x['replace']:
                            logging.info(f"{section}: REPLACE-WITH-EMPTY-FILE -> {file}:")
                        elif isinstance(x, dict) and 'replace' in x:
                            log_code_block(section, "REPLACE-FILE-WITH-CODE-BLOCK", file, x['replace'])
                        else:
                            logging.warning(f"Search and replace code blocks not found or incomplete for {file}")
                else:
                    logging.warning(f"{section}: REPLACE-CODE-BLOCK -> {key}: {value} - File not found")


def setup_logging():
    """
    Setup basic logging configuration.
    """
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


def parse_arguments():
    """
    Parse command-line arguments.

    Returns:
        argparse.Namespace: Parsed arguments.
    """
    parser = argparse.ArgumentParser(description='Scan and process directory files based on YAML configuration rules.')
    parser.add_argument('source', help='Path to the source directory to scan.')
    parser.add_argument('-c', '--config', help='Path to YAML configuration file.', required=False)
    parser.add_argument('--debug', help='Enable debug mode to only print files without copying.', action='store_true')
    return parser.parse_args()


def find_config_in_project(default_name='config.yaml'):
    """
    Look for a default configuration file in the current and parent directories.

    Args:
        default_name (str): Default configuration file name.

    Returns:
        str: Path to the configuration file.
    """
    current_dir = os.path.dirname(os.path.abspath(__file__))
    config_path = os.path.join(current_dir, default_name)
    if os.path.exists(config_path):
        return config_path
    parent_dir = os.path.dirname(current_dir)
    config_path = os.path.join(parent_dir, default_name)
    if os.path.exists(config_path):
        return config_path
    return None


def main():
    setup_logging()
    args = parse_arguments()

    config_path = args.config if args.config else find_config_in_project()
    if not config_path or not os.path.exists(config_path):
        logging.error(
            "Configuration file not found. Please provide a configuration file or ensure one exists in the project "
            "directory.")
        return

    source_directory = args.source
    config = load_config(config_path)
    output_base = os.path.join("OUT")

    # Cleanup only if not in debug mode
    if not args.debug:
        try:
            shutil.rmtree(output_base)
            logging.info("Previous output directory removed.")
        except FileNotFoundError:
            logging.info("No previous output directory to remove.")

    # Process files based on Solution and Stubs configurations
    scan_and_process_directory(config, 'SOLUTION', source_directory, output_base, args.debug)
    scan_and_process_directory(config, 'STUBS', source_directory, output_base, args.debug)


if __name__ == "__main__":
    main()
