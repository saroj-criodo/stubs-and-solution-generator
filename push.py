#!/usr/bin/env python

import git
import os
import requests
import logging
from argparse import ArgumentParser
from git_config_reader import main as read_git_config

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def fetch_namespace_id(token, group_name):
    """Fetch the namespace ID for a given group name, case-insensitively."""
    url = "https://gitlab.com/api/v4/namespaces?search=" + group_name.lower()
    headers = {"Private-Token": token}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        namespaces = response.json()
        for namespace in namespaces:
            if namespace['path'].lower() == group_name.lower():
                logging.info(f"Found namespace ID for group: {namespace['id']}")
                return namespace['id']
    logging.error(f"Failed to fetch namespace ID for group: {group_name}")
    return None

def create_repo(token, namespace_id, repo_name):
    """Create a new repository in GitLab using the namespace ID, with visibility set based on the repo_name."""
    url = "https://gitlab.com/api/v4/projects"
    headers = {"Private-Token": token}
    visibility = "public" if "STUBS" in repo_name else "private"
    data = {
        "name": repo_name,
        "namespace_id": namespace_id,
        "visibility": visibility
    }
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 201:
        logging.info(f"Repository {repo_name} created successfully with {visibility} visibility.")
        return response.json()['http_url_to_repo']
    else:
        logging.error(f"Failed to create repository {repo_name}. Response: {response.json()}")
        return None

def setup_remote_and_push(repo_path, repo_url, new_repo, user_email, user_name):
    """Set up remote origin and push to the repository, with new_repo flag to distinguish actions.
       Use user_email and user_name for commits, if provided."""
    try:
        repo = git.Repo.init(repo_path)
        repo.config_writer().set_value("user", "email", user_email).release()
        repo.config_writer().set_value("user", "name", user_name).release()

        repo.git.add(all=True)
        commit_message = "Updating repository" if not new_repo else "Initial commit"
        repo.index.commit(commit_message)

        if 'origin' in repo.remotes:
            origin = repo.remotes.origin
        else:
            origin = repo.create_remote('origin', repo_url)

        origin.fetch()

        # Additional operations for new repositories or updates
        if new_repo and not origin.refs:
            logging.info("Remote repository is empty. Proceeding to push.")
        else:
            logging.info("Rebasing before pushing.")
            repo.git.rebase('origin/master')

        repo.git.push('--set-upstream', origin.name, repo.active_branch)
        logging.info(f"{'Updated' if not new_repo else 'Pushed'} repository successfully.")
    except git.exc.GitCommandError as e:
        logging.error(f"Error in rebasing or pushing: {e}")

def check_repo_exists(token, namespace_id, repo_name):
    """Check if the repository already exists on GitLab."""
    url = f"https://gitlab.com/api/v4/groups/{namespace_id}/projects"
    headers = {"Private-Token": token}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        projects = response.json()
        for project in projects:
            if project['name'] == repo_name:
                logging.info(f"Repository {repo_name} exists on GitLab.")
                return True
        logging.info(f"Repository {repo_name} does not exist on GitLab.")
        return False
    else:
        logging.error(f"Failed to retrieve projects. Status code: {response.status_code}")
        return False

def process_repository(token, group_path, group_name, repo_name):
    """Process each repository directory, enhanced with repo existence check and user information for commits."""
    namespace_id = fetch_namespace_id(token, group_name)
    if not namespace_id:
        return

    user_email, user_name = read_git_config()
    if not user_email or not user_name:
        logging.warning("Git config did not return user information. Using default commit settings.")
        user_email = "generatorbot@criodo.com"
        user_name = "GeneratorBot"

    repo_path = os.path.join(group_path, repo_name)
    if check_repo_exists(token, namespace_id, repo_name):
        repo_url = f"https://oauth2:{token}@gitlab.com/{group_name}/{repo_name}.git"
        setup_remote_and_push(repo_path, repo_url, new_repo=False, user_email=user_email, user_name=user_name)
    else:
        repo_url_api = create_repo(token, namespace_id, repo_name)
        if repo_url_api:
            repo_url_with_token = f"https://oauth2:{token}@gitlab.com/{group_name}/{repo_name}.git"
            setup_remote_and_push(repo_path, repo_url_with_token, new_repo=True, user_email=user_email, user_name=user_name)

def main(token, group_path, group_name):
    if not os.path.isdir(group_path):
        logging.error(f"Invalid path: {group_path}")
        return

    for repo_name in os.listdir(group_path):
        process_repository(token, group_path, group_name, repo_name)

if __name__ == "__main__":
    parser = ArgumentParser(description="Script to push repos to GitLab.")
    parser.add_argument("-t", "--token", required=True, help="Auth token for GitLab")
    parser.add_argument("-g", "--group", required=True, help="GitLab group name")
    parser.add_argument("-p", "--path", required=True, help="Path to the group folder containing the repos")

    args = parser.parse_args()
    main(args.token, args.path, args.group)

