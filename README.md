# STUBS and SOLUTION Generator

This tool helps you generate STUBS and SOLUTION configurations for your project based on a provided `config.yaml` file.

## Usage

1. Create a `config.yaml` file with the required configurations. Here's an example:
   ```yaml
   # ME Details
   ME: ME_EXAMPLE
   MODULE: MODULE_EXAMPLE
   
   GLOBAL:
     # Things you wish to INCLUDE files/folders for both SOLUTION and STUBS
     INCLUDE:
       - "src/"
       - "build.gradle"
       - "gradlew"
       - "Dockerfile"
     # Things you wish to ignore inside the folders mentioned in INCLUDE
     IGNORE:
       - "src/main/java/com/example/app/experiments"
     # Lines you want to remove from files from both SOLUTION and STUBS, lines not needed to come together in the file
     REMOVE-LINES:
       "src/main/java/com/example/app/config/AppConfig.java":
         - "import com.example.app.legacy.LegacyService;"
         - "private final LegacyService legacyService;"
     # CODE-BLOCK you want to remove from files from both SOLUTION and STUBS, lines need to come together in the file
     REMOVE-CODE-BLOCK:
       "src/main/java/com/example/app/service/MainService.java": |
         // Legacy code block
         if (legacyService.isLegacyModeEnabled()) {
           return legacyService.performLegacyOperation();
         }
     # CODE BLOCK uou want to replace from files from  both SOLUTION and STUBS
     REPLACE-CODE-BLOCK:
       "src/main/java/com/example/app/controller/MainController.java":
         - search: |
             // Old implementation
             String response = mainService.performOperation(request);
             return ResponseEntity.ok(response);
           replace: |
             // New implementation
             Response response = mainService.performOperationV2(request);
             return ResponseEntity.ok(response.getData());
     # CODE-BLOCK you want to replace with in the files in BOTH SOLUTION AND STUBS
     REPLACE-CODE-BLOCK:
       "src/main/java/com/example/app/controller/MainController.java":
         - replace: |
             // New implementation
             Response response = mainService.performOperationV2(request);
             return ResponseEntity.ok(response.getData());
     # REPLACE WITH EMPTY files in BOTH SOLUTION AND STUBS
     REPLACE-CODE-BLOCK:
       "src/main/java/com/example/app/controller/MainController.java":
         - replace: {}
   
   SOLUTION:
     INCLUDE: []
     IGNORE: []
     REMOVE-LINES: {}
     REMOVE-CODE-BLOCK: {}
     REPLACE-CODE-BLOCK: {}
   
   STUBS:
     INCLUDE: []
     IGNORE: []
     REMOVE-LINES: {}
     REMOVE-CODE-BLOCK: {}
     REPLACE-CODE-BLOCK: {}
   ```
2. Run the `generator.py` script with the following commands:
   ```commandline
    # To check what changes will be made (dry run)
    ./generator.py <FOLDER_TO_GENERATE_FROM> --debug
    
    # To actually generate SOLUTION and STUBS configurations
    ./generator.py <FOLDER_TO_GENERATE_FROM>
    
    # Optional: Provide a custom path to the config.yaml file, 
    # by default config file is checked in current location
    ./generator.py <FOLDER_TO_GENERATE_FROM> -c /path/to/config.yaml
    ```
Replace <FOLDER_TO_GENERATE_FROM> with the path to the folder containing your project files.

## Push to Gitlab
```commandline
./push.py -t $GITLAB_TOKEN -g <GROUP_NAME> -p <LOCAL_PATH_TO_GROUP>
```
